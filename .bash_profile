alias bashrc="notepad++ ~/.bash_profile"

alias git-bash="/git-bash.exe & > /dev/null 2>&1"
alias new="git-bash && exit"

alias g="git"
alias ga="git add"
alias gaa="git add *"
alias gc="git commit"
alias gcm="git commit -m"
alias gcam="git commit -am"
alias gst="git status"
alias glsf="git ls-tree --full-tree -r --name-only HEAD"
alias gl="git log --oneline"